#!/usr/bin/env python3

#create the connection to the database
import mysql.connector

#object to connect to the database
db = mysql.connector.connect(host='localhost', user='root', password='1', database='mysql')

#cursor to go through the database
cur = db.cursor()

cur.execute('SHOW TABLES')

for table in cur:
	print(table)

