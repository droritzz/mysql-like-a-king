#!/usr/bin/env python3

#create the connection to the database
import mysql.connector

#object to connect to the database
db = mysql.connector.connect(host='localhost', user='root', password='1', database='class')

#cursor to go through the database
cur = db.cursor()

cur.execute('CREATE TABLE class_mates(id INT, name VARCHAR(40), email VARCHAR(40))')
