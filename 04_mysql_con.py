#!/usr/bin/env python3

#create the connection to the database
import mysql.connector

#object to connect to the database
db = mysql.connector.connect(host='localhost', user='root', password='1', database='class')

#cursor to go through the database
cur = db.cursor()

#commented out, since we've created a table in the script before
#cur.execute('CREATE TABLE class_mates(id INT, name VARCHAR(41), email VARCHAR(40))')

#declare a list with values
# %s - placeholder for a string, %d - placeholder for a decimal integer
sql = "INSERT INTO class_mates(id, name, email) VALUES (%s, %s, %s)"

#declare a tuple variable
val = (1, 'Rick', 'screwthefederation@sanchez.rick')

#enter the data into the database
cur.execute(sql, val)

#checks if the data is in the database
db.commit()

#print the number of the new lines inserted into the database
print(cur.rowcount, "rows inserted")
